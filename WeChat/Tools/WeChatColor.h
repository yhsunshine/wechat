//
//  WeChatColor.h
//  WeChat
//
//  Created by yh_SunShine on 2016/9/23.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface WeChatColor : NSObject


UIColor* hexColor(NSString*hexColor);



@end
