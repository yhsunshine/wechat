//
//  ImageButton.swift
//  WeChat
//
//  Created by yh_SunShine on 15/12/8.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class ImageButton: UIButton {
    
    var iv:UIImageView!
    var label:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        iv=UIImageView(frame: CGRect(x: 20, y: frame.height/6.0, width: frame.height*2.0/3.0, height: frame.height*2.0/3.0))
        iv.contentMode=UIViewContentMode.scaleAspectFit
        self.addSubview(iv)
        
        label=UILabel(frame: CGRect(x: iv.right+5,y: 0,width: frame.width-iv.width-5,height: frame.height))
        self.addSubview(label)
        label.textColor=UIColor.white
        label.font=UIFont.systemFont(ofSize: 15)
    }
    
    func  setImageAndTitle(_ image:UIImage,title:String){
    
        iv.image=image
        label.text=title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   

}
