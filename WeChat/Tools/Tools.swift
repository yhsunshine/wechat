//
//  Tools.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class Tools: NSObject {

    class func colorWith(_ R:CGFloat,G:CGFloat,B:CGFloat)->UIColor{
    
        return UIColor(red: R/255.0, green: G/255.0, blue: B/255.0, alpha:1.0)
    }
    
    
    class func getCurrentVC()->UIViewController{
    
        let result:UIViewController?
        var window=UIApplication.shared.keyWindow
        if window?.windowLevel != UIWindowLevelNormal {
        
            let windows=UIApplication.shared.windows
            for childWindow in windows {
                if childWindow.windowLevel == UIWindowLevelNormal {
                    window!=childWindow
                    break
                }
            }
        }
        
        let frontView=window?.subviews.first
        let nextResponder=frontView?.next
        
        if nextResponder!.isKind(of: UIViewController.self) {
        
            result=nextResponder as? UIViewController
        }else{
        
            result=window?.rootViewController
        }
        return result!
        
    }
    
    class func getDateStr()->NSNumber{
    
        let date=Date()
        
        let timeSp = date.timeIntervalSince1970
        
       // print(timeSp)
//        NSDate* date = [NSDate date];
//        NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
        
       // let time = Int(timeSp)
        
//        let time:NSString=NSString(format: "%d", timeSp)
        
        return NSNumber(value: timeSp)

        
    }
}
