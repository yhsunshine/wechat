//
//  XmppManager.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit
import CoreData

enum ConnectType{
    case login
    case register
}

class XmppManager: NSObject,XMPPStreamDelegate,XMPPRosterDelegate,UIAlertViewDelegate{

//    private static var __once: () = { () -> Void in
//            manager.xmppManager=XmppManager()
//        }()
    /**
     * 单例
     */
    //    class func sharedXmppManager() ->XmppManager{
    //
    //        struct manager{
    //            static var xmppManager:XmppManager!
    //            static var once :Int=0
    //        }
    //
    //        disp
    //
    //        _ = XmppManager.__once
    //
    //        return manager.xmppManager
    //    }
    
    static let sharedXmppManager = XmppManager()
    
     var password:String?
     var username:String?
     var connectType:ConnectType?
    //通讯对象
    var xmppStream:XMPPStream?
    //联系人管理对象
    var xmppRoster:XMPPRoster?
    //信息归档对象
    var xmppMessageArchiving:XMPPMessageArchiving?
    
    
    /**
     * 重写初始化方法
     */
    override private init() {
        super.init()
        
        self.xmppStream=XMPPStream();
        self.xmppStream?.hostName=kHostName
        self.xmppStream?.hostPort=UInt16(kHostPort)
        self.xmppStream?.addDelegate(self, delegateQueue: DispatchQueue.main)
        
        let xmppRosterStorage=XMPPRosterCoreDataStorage.sharedInstance()
        self.xmppRoster=XMPPRoster(rosterStorage: xmppRosterStorage)
        self.xmppRoster?.activate(self.xmppStream)
        self.xmppRoster?.addDelegate(self, delegateQueue: DispatchQueue.main)
        
        let xmppMessageArchivingStorage=XMPPMessageArchivingCoreDataStorage.sharedInstance()
        self.xmppMessageArchiving=XMPPMessageArchiving(messageArchivingStorage: xmppMessageArchivingStorage, dispatchQueue: DispatchQueue.main)
        self.xmppMessageArchiving?.activate(self.xmppStream)
        
    }

    /**
     * 登录方法
     */
    func loginWithUserName(_ name:String,password:String){
    
        self.username=name
        
        self.password=password;
        
        self.connectType=ConnectType.login
        
        connectToServerWithUserName(name)
        
        
    }
    
    /**
     * 注册方法
     */
    func registerWithUserName(_ name:String,password:String){
    
        self.username=name
        
        self.password=password
        
        self.connectType=ConnectType.register
        
        connectToServerWithUserName(name)
    }
    
    /**
     * 连接服务器
     */
     func connectToServerWithUserName(_ name:String){
        
        let jid = XMPPJID.init(user: name, domain: kDomin, resource: kResouce)
        
        
//        let jid:XMPPJID
    
//        //创建Jid对象
//        let jid:XMPPJID=XMPPJID.withUser(name, domain: kDomin, resource: kResouce)
//        
        //设置Jid
        self.xmppStream?.myJID=jid
        //开始连接
        if self.xmppStream!.isConnected()||self.xmppStream!.isConnecting(){
            //先发送下线状态
            let presence=XMPPPresence(type: XIAXIAN);
            self.xmppStream?.send(presence)
            //断开连接
            self.xmppStream?.disconnect()
        }
        
        //向服务器发送请求

//        try! self.xmppStream?.connectWithTimeout(-1)
        
        do {
            try self.xmppStream?.connect(withTimeout: -1)
            
        }catch let error as NSError{
            
            print(error.localizedDescription)
            print("%s",#line);
        }
    }
    /**
     * 连接超时
     */
    func xmppStreamConnectDidTimeout(_ sender: XMPPStream!) {
        
        print("连接失败")
    }
    /**
     * 连接失败
     */
    func xmppStreamDidConnect(_ sender: XMPPStream!) {
        
        print("连接成功",#function)

        switch self.connectType!{
        
        case .login:
            //登录
            try! self.xmppStream?.authenticate(withPassword: self.password!)
            break;
        case .register:
            //注册
            try! self.xmppStream?.register(withPassword: self.password)
            break;
        }
    }
    
    /**
     * 登录成功
     */
    func xmppStreamDidAuthenticate(_ sender: XMPPStream!) {
        print("登录成功",#function)
        let presence=XMPPPresence(type: ZAIXIAN);
        self.xmppStream?.send(presence)
        
        //存储用户名
        UserDefaults.standard.set(String(self.username!+"@"+kDomin), forKey: "my_jid")
//        NSUserDefaults.standardUserDefaults().synchronize()
        UserDefaults.standard.set(self.username, forKey: "username")
        UserDefaults.standard.set(self.password, forKey: "password")
        UserDefaults.standard.synchronize()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: WeChatLoginSuccess), object: nil)
    }
    /**
     * 注册成功
     */
    func xmppStreamDidRegister(_ sender: XMPPStream!) {
        print("注册成功",#function)
        try! self.xmppStream?.authenticate(withPassword: self.password!)
    }
    
    func xmppRoster(_ sender: XMPPRoster!, didReceivePresenceSubscriptionRequest presence: XMPPPresence!) {
        
        print("在线收到添加好友消息")
        let alert=UIAlertController(title: "有新好友", message: presence.from().user, preferredStyle: UIAlertControllerStyle.alert)
        let agreeAction=UIAlertAction(title: "同意", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            //添加好友发送给服务器
            self.xmppRoster?.acceptPresenceSubscriptionRequest(from: presence.from(), andAddToRoster: true)
//            //写到到数据库
             ContactManager.sharedManager.insertContact(UserDefaults.standard.object(forKey: "my_jid") as! String, userJid: presence.fromStr(), userName: presence.from().user)
            //写入完毕，刷新好友列表
            NotificationCenter.default.post(name: Notification.Name(rawValue: UpDateContactList), object: nil)
            
        }
        let cancelAction=UIAlertAction(title: "拒绝", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            self.xmppRoster?.rejectPresenceSubscriptionRequest(from: presence.from())
        }
        alert.addAction(agreeAction)
        alert.addAction(cancelAction)
        TabbarViewController.sharedTabbar.present(alert, animated: true, completion: nil)
        
    }
    
    func xmppStream(_ sender: XMPPStream!, didReceive presence: XMPPPresence!) {
        
        /*
        print(__FUNCTION__)
        
        let presenceType=presence.type()
        let presenceUser=presence.from().user
        print(presenceType,presenceUser)
        if presenceType == "subscribed" {
        print("再次处理添加好友")
            self.xmppRoster?.acceptPresenceSubscriptionRequestFrom(presence.from(), andAddToRoster: true)
        }*/
    }
    
    func xmppRosterDidBeginPopulating(_ sender: XMPPRoster!) {
//        print("开始搜索好友",__FUNCTION__)
    }
    
    func xmppRoster(_ sender: XMPPRoster!, didReceiveRosterItem item: DDXMLElement!) {
        print("搜索好友",item)
        let jid=item.attribute(forName: "jid").stringValue()
         var name:String="admin"
        if (item.attribute(forName: "name") != nil) {
            name=item.attribute(forName: "name").stringValue()
        }
        //搜索到好友，判断是否已经写入过数据库
        if !ContactManager.sharedManager.selectContactIsSave(jid!) {
           // 写入到数据库
            ContactManager.sharedManager.insertContact(UserDefaults.standard.object(forKey: "my_jid") as! String, userJid: jid!, userName: name)
        }
        
        
    }
    
    func xmppRosterDidEndPopulating(_ sender: XMPPRoster!) {
        
        //写入完毕，刷新好友列表
        NotificationCenter.default.post(name: Notification.Name(rawValue: UpDateContactList), object: nil)

        print("结束搜索好友",#function)

    }
    
    
    func xmppStream(_ sender: XMPPStream!, didReceive message: XMPPMessage!) {
        
        
        if (message.forName("body") != nil) {
            
            //获得聊天的内容
            let messageBody = message.forName("body").stringValue()
            //获得聊天的发件人
            let messageFrom = message.attribute(forName: "from").stringValue()
            
//            print(messageBody!+"\n"+messageFrom!)
            
//            print(message)
            
            let jidFrom = messageFrom?.components(separatedBy: "/")[0]
            
//            print(jidFrom)
            
            //获得聊天的发件人
            let jidTo = message.attribute(forName: "to").stringValue()
            
            //插入消息到数据库
            MessageManager.sharedManager.insertMes(jidFrom!, jidTo: jidTo!, content: messageBody!)
            
            //判断是否存在chatList对象
            let bool = ChatListManager.sharedManager.selectedChatListBy(jidFrom!, jidTo: jidTo!)
            
            if bool==false {
                
                //如果不存在，添加到聊天列表的数据库
                ChatListManager.sharedManager.insertChatList(jidFrom!, jidTo: jidTo!, content: messageBody!)
            }else{
            
                //如果存在，更新聊天列表对象
                ChatListManager.sharedManager.upDateChatListBy(jidFrom!, jidTo: jidTo!, content: messageBody!)
            }
            
            NotificationCenter.default.post(NSNotification(name: Notification.Name(UpDateChatList), object: nil) as Notification)
            
        }
        

        
        
    }


}
