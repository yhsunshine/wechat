//
//  ChatList.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/8.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import Foundation
import CoreData


class ChatList: NSManagedObject {

    @NSManaged var content: String?
    @NSManaged var time: NSNumber?
    @NSManaged var jid: String?
    @NSManaged var chat_name: String?
    
    
// Insert code here to add functionality to your managed object subclass

}
