//
//  User.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/2.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
class User: NSManagedObject {

    @NSManaged var jid: String?
    @NSManaged var contact: Contact?
// Insert code here to add functionality to your managed object subclass

}
