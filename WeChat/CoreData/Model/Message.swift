//
//  Message.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/15.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import Foundation
import CoreData


class Message: NSManagedObject {
    
    @NSManaged var time: NSNumber?
    @NSManaged var jid: String?
    @NSManaged var user: String?
    @NSManaged var content: String?
    
    // Insert code here to add functionality to your managed object subclass
    
}
