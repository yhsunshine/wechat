//
//  Contact.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/2.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import Foundation
import CoreData

@objc(Contact)
class Contact: NSManagedObject {

    @NSManaged var jid: String?
    @NSManaged var name: String?
    @NSManaged var user: User?
// Insert code here to add functionality to your managed object subclass

}
