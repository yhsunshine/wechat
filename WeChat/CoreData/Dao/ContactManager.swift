//
//  ContactManager.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/30.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit



class ContactManager: NSObject {
    
    static let sharedManager = ContactManager()
    
//    private static var __once: () = { () -> Void in
//            contact.manager=ContactManager()
//        }()
//    
//    class func sharedManager()->ContactManager{
//        
//        struct contact{
//            static var manager:ContactManager?
//            static var once:Int=0
//        }
//        _ = ContactManager.__once
//        return contact.manager!
//    }
    /**
     * 存储联系人
     */
    func insertContact(_ myJid:String,userJid:String,userName:String?){
        print("开始写入数据库")
        let context=CoreDataManager.sharedManager.managedObjectContext
        let user=NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as! User
        user.jid=myJid
        let contact=NSEntityDescription.insertNewObject(forEntityName: "Contact", into: context) as! Contact
        contact.jid=userJid
        contact.name=userName
        user.contact=contact
        CoreDataManager.sharedManager.saveContext()
        print("写入完毕")
    }
    
    /**
     * 通过my_jid查询所有联系人
     */
    func selectAllContacts(_ myJid:String)->Array<User>{
        
        var contacts:Array<User>?
        let context=CoreDataManager.sharedManager.managedObjectContext
        let request:NSFetchRequest<NSFetchRequestResult>=NSFetchRequest()
        request.entity=NSEntityDescription.entity(forEntityName: "User", in: context)
        request.predicate=NSPredicate(format: "jid = %@", myJid)
        
        do {
            
            contacts=try context.fetch(request) as? Array<User>
            
        }catch{
            
            contacts=Array()
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        
        return contacts!
    }
    /**
     * 联系人是否已经存在本地数据库
     */
    func selectContactIsSave(_ userJid:String)->Bool{
        var array:Array<AnyObject>?
        let context=CoreDataManager.sharedManager.managedObjectContext
        let request:NSFetchRequest<NSFetchRequestResult>=NSFetchRequest()
        request.entity=NSEntityDescription.entity(forEntityName: "Contact", in: context)
        request.predicate=NSPredicate(format: "jid = %@", userJid)
        
        do{
            
            array=try context.fetch(request)
            
        }catch{
            print(error as NSError)
        }
        
        if (array != nil && (array?.count)!>0) {
            return true
        }
        return false
    }
    
}
