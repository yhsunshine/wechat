//
//  ChatListManager.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/8.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import UIKit

class ChatListManager: NSObject {
    
    static let sharedManager = ChatListManager()
    
    //    private static var __once: () = { () -> Void in
    //            chatList.manager=ChatListManager()
    //        }()
    //
    //    class func sharedManager()->ChatListManager{
    //
    //        struct chatList{
    //            static var manager:ChatListManager?
    //            static var once:Int=0
    //        }
    //        _ = ChatListManager.__once
    //        return chatList.manager!
    //    }
    
    
    /**
     * 查询聊天列表
     */
    func selectAllChatLists() -> Array<ChatList> {
        
        var chatLists : Array<AnyObject>?
        
        let context = CoreDataManager.sharedManager.managedObjectContext
        
        let reuqest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "ChatList")
        
        let entity = NSEntityDescription.entity(forEntityName: "ChatList", in: context)
        
        reuqest.entity=entity
        
        do{
            
            chatLists=try context.fetch(reuqest)
            
        }catch{
            
            print(error as NSError)
        }
        
        
        return chatLists as! Array<ChatList>
        
        
    }
    
    
    /**
     * 插入聊天列表
     */
    func insertChatList(_ jidFrom:String,jidTo:String,content:String) -> Void {
        
        let context = CoreDataManager.sharedManager.managedObjectContext
        
        let chat = NSEntityDescription.insertNewObject(forEntityName: "ChatList", into: context) as! ChatList
        
        let jid = jidFrom+jidTo
        
        chat.jid=jid
        chat.chat_name="哈哈";
        chat.time=Tools.getDateStr()
        chat.content=content
        
        CoreDataManager.sharedManager.saveContext()
        
    }
    
    /**
     * 通过jid查询聊天列表是否存在
     */
    
    func selectedChatListBy(_ jidFrom:String,jidTo:String)->Bool{
        
        var contacts:Array<ChatList>?
        
        var chatList:ChatList?
        
        let jid = jidFrom+jidTo
        
        let context = CoreDataManager.sharedManager.managedObjectContext
        
        let entity = NSEntityDescription.entity(forEntityName: "ChatList", in: context)
        
        let request:NSFetchRequest<NSFetchRequestResult>=NSFetchRequest()
        
        request.entity=entity
        
        request.predicate=NSPredicate(format: "jid == %@", jid)
        
        do {
            
            contacts  =  try context.fetch(request) as? Array<ChatList>
            
            
        } catch  {}
        
        if (contacts?.count)!>0 {
            chatList=contacts![0]
            return true
        }
        
        return false
        
    }
    
    
    /**
     * 更新chatList对象
     */
    func upDateChatListBy(_ jidFrom:String,jidTo:String,content:String)->Void{
        
        var chatList:ChatList?
        
        var chatLists:Array<ChatList>?
        
        let jid = jidFrom+jidTo
        
        let context = CoreDataManager.sharedManager.managedObjectContext
        
        let entity = NSEntityDescription.entity(forEntityName: "ChatList", in: context)
        
        let request:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest()
        
        request.entity=entity
        
        request.predicate=NSPredicate(format: "jid == %@", jid)
        
        do {
            chatLists =  try context.fetch(request) as? Array<ChatList>
            chatList=chatLists![0]
        } catch  {}
        
        if chatList != nil {
            chatList?.content=content
            chatList?.time=Tools.getDateStr()
            CoreDataManager.sharedManager.saveContext()
        }
        
        
        
        
        
        
        
    }
    
}
