//
//  MessageManager.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/6.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import UIKit

class MessageManager: NSObject {
    
    static let sharedManager = MessageManager()

//    private static var __once: () = { () -> Void in
//            message.manager=MessageManager()
//        }()
//
//    class func sharedManager()->MessageManager{
//        
//        struct message{
//            static var manager:MessageManager?
//            static var once:Int=0
//        }
//        _ = MessageManager.__once
//        return message.manager!
//    }

    //MARK: 查询所有聊天记录
    func selectedAllMes() -> Array<Message> {
       
        var mes:Array<Message>?
        let context=CoreDataManager.sharedManager.managedObjectContext
        let request:NSFetchRequest<NSFetchRequestResult>=NSFetchRequest()
        
        request.entity=NSEntityDescription.entity(forEntityName: "Message", in: context)
        
        do {
            mes=try context.fetch(request) as? Array<Message>
        } catch {
            mes=Array()
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        
        return mes!
    }
    
    
    /**
     * 通过jid查询聊天记录
     * jidFrom 消息来自哪里
     * jidTo   消息给向谁(一般都是自己)
     */
    // MARK: 通过jid查询聊天记录
    func selectedMes(_ jidFrom:String,jidTo:String)->Array<Message>{
    
        var mes:Array<Message>?
        let context=CoreDataManager.sharedManager.managedObjectContext
        let request:NSFetchRequest<NSFetchRequestResult>=NSFetchRequest()
        let jid = jidFrom+jidTo
        
        request.entity=NSEntityDescription.entity(forEntityName: "Message", in: context)
        
        request.predicate=NSPredicate(format: "jid == %@", jid)
        
        do {
            
            mes=try context.fetch(request) as? Array<Message>
            
        }catch{
            
            mes=Array()
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
        
        return mes!
    
    }
    
    /**
     * 插入消息
     * jidFrom 消息来自哪里
     * jidTo   消息给向谁(一般都是自己)
     * content 内容
     */
    func insertMes(_ jidFrom:String,jidTo:String,content:String){
    
//        print("开始写入数据库",#function)
        let context=CoreDataManager.sharedManager.managedObjectContext
        let message=NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as! Message
        let jid = jidFrom+jidTo
        message.content=content
        message.jid=jid
        message.user=jidFrom
        message.time=Tools.getDateStr()
        CoreDataManager.sharedManager.saveContext()
//        print("\(message.content)"+"的时间是："+"\(message.time)"+#function)
//        print("写入消息完毕")
    }
    
}
