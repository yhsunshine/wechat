//
//  LoginViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/26.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate{

    var userTextField:UITextField!
    var passwordTextField:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        
        addNotifications()

        // Do any additional setup after loading the view.
    }
    
    
    func initUI(){
    
        let label=UILabel(frame: CGRect(x: 0,y: 100,width: self.view.frame.width,height: 100));
        self.view.addSubview(label);
        label.text="登录账户";
        label.font=UIFont(name: FontFamilyName, size: 30);
        label.textAlignment=NSTextAlignment.center;
//        label.font=UIFont.systemFontOfSize(30)
        
        let userNameLabel=UILabel(frame: CGRect(x: 20,y: label.bottom,width: 50,height: 50));
        self.view.addSubview(userNameLabel);
        userNameLabel.text="账户";
//        userNameLabel.font=UIFont(name: FontFamilyName, size: 15);
        userNameLabel.font=UIFont.systemFont(ofSize: 15)
        
        userTextField=UITextField(frame: CGRect(x: userNameLabel.right,y: userNameLabel.top,width: self.view.width-userNameLabel.right,height: userNameLabel.height));
        self.view.addSubview(userTextField);
        userTextField.placeholder="微信号"
//        userTextField.font=UIFont(name: FontFamilyName, size: 15);
         userTextField.font=UIFont.systemFont(ofSize: 15)
        userTextField.clearButtonMode=UITextFieldViewMode.always;
        userTextField.delegate=self
        
        let middleLine=UIView(frame: CGRect(x: userNameLabel.left,y: userNameLabel.bottom,width: self.view.width-userNameLabel.left,height: 1));
        self.view.addSubview(middleLine);
        middleLine.backgroundColor=UIColor(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0);
        
        let passwordLabel=UILabel(frame: CGRect(x: 20,y: middleLine.bottom,width: 50,height: 50));
        self.view.addSubview(passwordLabel);
        passwordLabel.text="密码";
//        passwordLabel.font=UIFont(name: FontFamilyName, size: 15);
        passwordLabel.font=UIFont.systemFont(ofSize: 15)
        
        passwordTextField=UITextField(frame: CGRect(x: passwordLabel.right,y: passwordLabel.top,width: self.view.width-passwordLabel.right,height: passwordLabel.height));
        self.view.addSubview(passwordTextField);
        passwordTextField.placeholder="请填写密码"
//        passwordTextField.font=UIFont(name: FontFamilyName, size: 15);
        passwordTextField.font=UIFont.systemFont(ofSize: 15)
        passwordTextField.isSecureTextEntry=true;
        passwordTextField.delegate=self
        
        let bottomLine=UIView(frame: CGRect(x: passwordLabel.left,y: passwordLabel.bottom,width: self.view.width-passwordLabel.left,height: 1));
        self.view.addSubview(bottomLine);
        bottomLine.backgroundColor=UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0);
        
        let loginButton=UIButton(frame: CGRect(x: 20,y: bottomLine.bottom+30,width: self.view.width-40,height: 44));
        self.view.addSubview(loginButton);
        
        var image=UIImage(named: "fts_green_btn");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        loginButton.setBackgroundImage(image, for: UIControlState())
        
        image=UIImage(named: "fts_green_btn_HL");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        loginButton.setBackgroundImage(image, for: UIControlState.highlighted)

//        loginButton.titleLabel?.font=UIFont(name: FontFamilyName, size: 17);
        loginButton.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        loginButton.setTitle("登录", for: UIControlState());
        loginButton.setTitleColor(UIColor.white, for: UIControlState());
        loginButton.addTarget(self, action: #selector(LoginViewController.loginButtonClick), for: UIControlEvents.touchUpInside);
        loginButton.layer.cornerRadius=5;
        loginButton.layer.masksToBounds=true;
        
        let moreButton=UIButton(frame: CGRect(x: 0,y: self.view.bottom-44,width: self.view.width,height: 44))
        self.view.addSubview(moreButton)
        moreButton .setTitle("更多", for: UIControlState())
        moreButton.setTitleColor(UIColor(red: 88.0/255.0, green: 108.0/255.0, blue: 148.0/256.0, alpha: 1.0), for: UIControlState())
        moreButton.addTarget(self, action: #selector(LoginViewController.moreButtonClick), for: UIControlEvents.touchUpInside)
       
    }
    
    func addNotifications(){
    
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.LoginSuccess), name: NSNotification.Name(rawValue: WeChatLoginSuccess), object: nil)
    }
    
    func LoginSuccess(){
        
        
        self.navigationController!.present(TabbarViewController.sharedTabbar, animated: true, completion: nil)
        
    }
    
    
    func moreButtonClick(){
    
       let alert=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let chooesUser=UIAlertAction(title: "切换账号...", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            print("切换账号")
        }
        let register=UIAlertAction(title: "注册", style: UIAlertActionStyle.default) { (UIAlertAction) -> Void in
            print("注册")
            self.navigationController?.pushViewController(RegisterViewController(), animated: true)
        }
        let forgetPassword=UIAlertAction(title: "忘记密码", style: UIAlertActionStyle.default) { (sender:UIAlertAction) -> Void in
            print("忘记密码")
        }
        let cancel=UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel) { (UIAlertAction) -> Void in
            print("取消")
        }
        alert.addAction(chooesUser)
        alert.addAction(register)
        alert.addAction(forgetPassword)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func loginButtonClick(){
        
        userTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        if !userTextField.text!.isEmpty && !passwordTextField.text!.isEmpty {
            XmppManager.sharedXmppManager.loginWithUserName(userTextField.text!, password: passwordTextField.text!)
        }else{
            print("账户密码不能为空")
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !userTextField.text!.isEmpty && !passwordTextField.text!.isEmpty {
            loginButtonClick()
            return true
        }
        return false
    }
    
    deinit{
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: WeChatLoginSuccess), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
