//
//  RegisterViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController,UITextFieldDelegate {

    
    var userTextField:UITextField!
    var passwordTextField:UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor=UIColor.white

        initUI()
        
//        XmppManager.sharedXmppManager().xmppStream?.addDelegate(self, delegateQueue: dispatch_get_main_queue())
        
        // Do any additional setup after loading the view.
    }
    
    func initUI(){
    
        let cancelButton=UIButton(type: UIButtonType.custom)
        cancelButton.frame=CGRect(x: 10, y: 30, width: 40, height: 25)
        self.view.addSubview(cancelButton)
//        cancelButton.titleLabel?.font=UIFont(name: FontFamilyName, size: 17);
        cancelButton.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        cancelButton.setTitle("取消", for: UIControlState())
        cancelButton.setTitleColor(UIColor(red: 0.0/255.0, green: 182.0/255.0, blue: 0.0/255.0, alpha: 1.0), for: UIControlState())
        cancelButton.addTarget(self, action: #selector(RegisterViewController.cancel), for: UIControlEvents.touchUpInside)
        
        let label=UILabel(frame: CGRect(x: 0,y: 100,width: self.view.frame.width,height: 100));
        self.view.addSubview(label);
        label.text="注册账户";
        label.font=UIFont(name: FontFamilyName, size: 30);
        label.textAlignment=NSTextAlignment.center;
        //        label.font=UIFont.systemFontOfSize(30)
        
        let userNameLabel=UILabel(frame: CGRect(x: 20,y: label.bottom,width: 50,height: 50));
        self.view.addSubview(userNameLabel);
        userNameLabel.text="账户";
        //        userNameLabel.font=UIFont(name: FontFamilyName, size: 15);
        userNameLabel.font=UIFont.systemFont(ofSize: 15)
        
        userTextField=UITextField(frame: CGRect(x: userNameLabel.right,y: userNameLabel.top,width: self.view.width-userNameLabel.right,height: userNameLabel.height));
        self.view.addSubview(userTextField);
        userTextField.placeholder="微信号"
        //        userTextField.font=UIFont(name: FontFamilyName, size: 15);
        userTextField.font=UIFont.systemFont(ofSize: 15)
        userTextField.clearButtonMode=UITextFieldViewMode.always;
        userTextField.delegate=self
        
        let middleLine=UIView(frame: CGRect(x: userNameLabel.left,y: userNameLabel.bottom,width: self.view.width-userNameLabel.left,height: 1));
        self.view.addSubview(middleLine);
        middleLine.backgroundColor=UIColor(red: 233.0/255.0, green: 233.0/255.0, blue: 233.0/255.0, alpha: 1.0);
        
        let passwordLabel=UILabel(frame: CGRect(x: 20,y: middleLine.bottom,width: 50,height: 50));
        self.view.addSubview(passwordLabel);
        passwordLabel.text="密码";
        //        passwordLabel.font=UIFont(name: FontFamilyName, size: 15);
        passwordLabel.font=UIFont.systemFont(ofSize: 15)
        
        passwordTextField=UITextField(frame: CGRect(x: passwordLabel.right,y: passwordLabel.top,width: self.view.width-passwordLabel.right,height: passwordLabel.height));
        self.view.addSubview(passwordTextField);
        passwordTextField.placeholder="请填写密码"
        //        passwordTextField.font=UIFont(name: FontFamilyName, size: 15);
        passwordTextField.font=UIFont.systemFont(ofSize: 15)
        passwordTextField.isSecureTextEntry=true;
        passwordTextField.delegate=self
        
        let bottomLine=UIView(frame: CGRect(x: passwordLabel.left,y: passwordLabel.bottom,width: self.view.width-passwordLabel.left,height: 1));
        self.view.addSubview(bottomLine);
        bottomLine.backgroundColor=UIColor(red: 223.0/255.0, green: 223.0/255.0, blue: 223.0/255.0, alpha: 1.0);
        
        let registerButton=UIButton(frame: CGRect(x: 20,y: bottomLine.bottom+30,width: self.view.width-40,height: 44));
        self.view.addSubview(registerButton);
        
        var image=UIImage(named: "fts_green_btn");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        registerButton.setBackgroundImage(image, for: UIControlState())
        
        image=UIImage(named: "fts_green_btn_HL");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        registerButton.setBackgroundImage(image, for: UIControlState.highlighted)
        
        //        loginButton.titleLabel?.font=UIFont(name: FontFamilyName, size: 17);
        registerButton.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        registerButton.setTitle("注册", for: UIControlState());
        registerButton.setTitleColor(UIColor.white, for: UIControlState());
        registerButton.addTarget(self, action: #selector(RegisterViewController.registerButtonClick), for: UIControlEvents.touchUpInside);
        registerButton.layer.cornerRadius=5;
        registerButton.layer.masksToBounds=true;

    }
    
    func cancel(){
    
        self.navigationController!.popViewController(animated: true)
    }
    
    func registerButtonClick(){
    
        userTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        if !userTextField.text!.isEmpty && !passwordTextField.text!.isEmpty {
            
             XmppManager.sharedXmppManager.registerWithUserName(self.userTextField.text!, password: self.passwordTextField.text!)
//            self.dismissViewControllerAnimated(true, completion: { () -> Void in
//                
//            })
           

        }else{
            print("账户密码不能为空")
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    deinit{
        print("页面消亡")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
