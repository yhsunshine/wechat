//
//  TabbarViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

    static let sharedTabbar = TabbarViewController()
    
//    private static var __once: () = { () -> Void in
//            tabbar.controller=TabbarViewController()
//        }()
//
//    
//    class func sharedTabbar()->TabbarViewController{
//    
//        struct tabbar{
//            static var controller:TabbarViewController!
//            static var once :Int=0
//        }
//        
//        _ = TabbarViewController.__once
//        
//        return tabbar.controller
//
//    }
//    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor=UIColor.white
        
        self.tabBar.tintColor=UIColor(red: 31.0/256.0, green: 185.0/256.0, blue: 34.0/256.0, alpha: 1.0)
        
        initUI()
        
        // Do any additional setup after loading the view.
    }
    
    func initUI(){
        //微信
        let chatViewController=ChatViewController()
        let chatItem=UITabBarItem(title: "微信", image: UIImage(named: "tabbar_mainframe"), selectedImage: UIImage(named: "tabbar_mainframeHL"))
        chatViewController.tabBarItem=chatItem
        let chatNav=NavigationViewController(rootViewController: chatViewController)
        
        //联系人
        let contactViewController=ContactViewController()
        let ContactItem=UITabBarItem(title: "通讯录", image: UIImage(named: "tabbar_contacts"), selectedImage: UIImage(named: "tabbar_contactsHL"))
        contactViewController.tabBarItem=ContactItem
        let contactNav=NavigationViewController(rootViewController: contactViewController)
        
        //发现
        let findViewController=FindViewController()
        let findItem=UITabBarItem(title: "发现", image: UIImage(named: "tabbar_discover"), selectedImage: UIImage(named: "tabbar_discoverHL"))
        findViewController.tabBarItem=findItem
        let findNav=NavigationViewController(rootViewController: findViewController)
        
        //我
        let accountViewController=AccountViewController()
        let accountItem=UITabBarItem(title: "我", image: UIImage(named: "tabbar_me"), selectedImage: UIImage(named: "tabbar_meHL"))
        accountViewController.tabBarItem=accountItem
        let accountNav=NavigationViewController(rootViewController: accountViewController)
        
        
        self.viewControllers=[chatNav,contactNav,findNav,accountNav]
    }
    
    
      
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
