//
//  NavigationViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.navigationBar.barTintColor=Tools.colorWith(34, G: 35, B: 35)
        
        self.navigationBar.titleTextAttributes=NSDictionary(objects: [UIColor.white,UIFont(name: FontFamilyName, size: 20)!], forKeys: [NSForegroundColorAttributeName as NSCopying,NSFontAttributeName as NSCopying]) as? [String : AnyObject]
        
        self.navigationBar.barStyle=UIBarStyle.black
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "aroundfriends_bar_bg"), forBarMetrics: UIBarMetrics.Default)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
