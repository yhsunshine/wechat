//
//  ChatDeatilViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/19.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import UIKit

//聊天详情页面
class ChatDeatilViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tableView:UITableView?
    var keyBoard :UIView?
    var textView :UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor=UIColor.white
        
        self.navigationController?.navigationBar.tintColor=UIColor.white
        
        self.navigationItem.setLeftBarButton(UIBarButtonItem(title: "< 返回", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatDeatilViewController.backButton)), animated: true)
        
        
        tableView=UITableView()
        tableView?.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height-(self.tabBarController?.tabBar.height)!)
        self.view.addSubview(tableView!)
        tableView?.delegate=self
        tableView?.dataSource=self
        tableView?.backgroundColor=hexColor("#ebebeb")
        
        
        
        keyBoard=UIView(frame: CGRect(x: 0, y: (tableView?.bottom)!, width: (tableView?.width)!, height: (self.tabBarController?.tabBar.height)!))
//        keyBoard?.backgroundColor=UIColor.init(red: 245/255, green: 245/255, blue: 246/255, alpha: 1.0)
        keyBoard?.backgroundColor=hexColor("#f5f5f6")
        self.view .addSubview(keyBoard!)
        
        let line = UIView(frame: CGRect(x: 0, y: 0, width: (keyBoard?.width)!, height: 0.5))
        line.backgroundColor=hexColor("#d7d7d9")
        keyBoard?.addSubview(line)
        
        
        textView=UITextView(frame: CGRect(x: 20, y: 7.5, width: self.view.width-40, height: (keyBoard?.height)!-15))
        keyBoard?.addSubview(textView!)
        textView?.backgroundColor=UIColor.white
        textView?.layer.cornerRadius=5
        textView?.layer.borderWidth=0.5
        textView?.layer.borderColor=hexColor("#dddddd").cgColor
        textView?.font=UIFont.systemFont(ofSize: 16)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatDeatilViewController.keyboardWillShow(_:)), name: NSNotification.Name("UIKeyboardWillShowNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatDeatilViewController.keyboardShow(_:)), name: NSNotification.Name("UIKeyboardDidShowNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatDeatilViewController.keyboardWillHide(_:)), name: NSNotification.Name("UIKeyboardWillHideNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatDeatilViewController.keyboardHide(_:)), name: NSNotification.Name("UIKeyboardDidHideNotification"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    
    func keyboardWillShow(_ not:NSNotification) -> Void {
        print("keyboardWillShow")
    }
    
    func keyboardShow(_ not:NSNotification) -> Void {
        print("keyboardShow")
    }
    
    func keyboardWillHide(_ not:NSNotification) -> Void {
        print("keyboardWillHide")
    }
    
    func keyboardHide(_ not:NSNotification) -> Void {
        print("keyboardHide")
    }
    
    
    func backButton(){
        
        self.tabBarController?.tabBar.isHidden=false
        
        self.navigationController!.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellID="cell"
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellID)
        
        if cell == nil {
            
            cell=UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellID)
            cell?.backgroundColor=UIColor.clear
        }
        
        return cell!
        
        
    }
    
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("UIKeyboardWillShowNotification"), object: self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("UIKeyboardDidShowNotification"), object: self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("UIKeyboardWillHideNotification"), object: self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("UIKeyboardDidHideNotification"), object: self)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
