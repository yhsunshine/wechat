//
//  AddFriendViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/12/8.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class AddFriendViewController: UIViewController {

    var textField:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor=Tools.colorWith(239, G: 239, B: 244)
        
        self.navigationController?.navigationBar.tintColor=UIColor.white
        
        
        initUI()
        // Do any additional setup after loading the view.
    }
    
  
    func initUI(){
    
        textField=UITextField(frame: CGRect(x: 0,y: 84,width: self.view.width,height: 44))
        self.view.addSubview(textField!)
        textField!.placeholder="微信号/QQ号/手机号"
        textField?.backgroundColor=UIColor.white
        
        let addFriendButton=UIButton(frame: CGRect(x: 20,y: (textField!.bottom)+30,width: self.view.width-40,height: 44));
        self.view.addSubview(addFriendButton);
        
        var image=UIImage(named: "fts_green_btn");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        addFriendButton.setBackgroundImage(image, for: UIControlState())
        
        image=UIImage(named: "fts_green_btn_HL");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        addFriendButton.setBackgroundImage(image, for: UIControlState.highlighted)
        
        //        addFriendButton.titleLabel?.font=UIFont(name: FontFamilyName, size: 17);
        addFriendButton.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        addFriendButton.setTitle("添加好友", for: UIControlState());
        addFriendButton.setTitleColor(UIColor.white, for: UIControlState());
        addFriendButton.addTarget(self, action: #selector(addFriendClick), for: UIControlEvents.touchUpInside);
        addFriendButton.layer.cornerRadius=5;
        addFriendButton.layer.masksToBounds=true;

        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textField?.resignFirstResponder()
    }
    
    func addFriendClick(){
        
        let jid=XMPPJID.init(user: self.textField?.text, domain: kDomin, resource: kResouce)
        XmppManager.sharedXmppManager.xmppRoster?.addUser(jid, withNickname: nil)
        self.navigationController!.popViewController(animated: true)
        print(#function)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit{
    
        print(#function,self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
