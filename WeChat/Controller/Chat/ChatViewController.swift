//
//  ChatViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var optionsView:UIImageView?
    
    var tableView:UITableView?
    
    lazy var mes:Array<ChatList> = {
        
        let array = Array<ChatList>()
        
        return array
        
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title="微信"
        
        
        self.navigationItem.rightBarButtonItem=UIBarButtonItem(image: UIImage(named: "barbuttonicon_add"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatViewController.addContact))
        self.navigationItem.rightBarButtonItem?.tintColor=UIColor.white
        
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(UpDateChatList), object: nil, queue: OperationQueue.main) { (Notification) in
            
            self.tableView?.reloadData()
            
        }
        
        initTableView()
        
        initUI()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: 初始化页面
    func initUI(){
        optionsView=UIImageView(frame: CGRect(x: self.view.width-140-5,y: 64,width: 140
            ,height: 190))
        self.view.addSubview(optionsView!)
        var image=UIImage(named: "MoreFunctionFrame")
        image=image?.resizableImage(withCapInsets: UIEdgeInsets(top: 25, left: 25, bottom: 30, right: 40), resizingMode: UIImageResizingMode.stretch)
        optionsView?.image=image
        optionsView?.alpha=0;
        optionsView?.isUserInteractionEnabled=true
        
        let titles=["发起群聊","添加朋友","扫一扫","收钱"]
        let images=["contacts_add_newmessage","contacts_add_friend","contacts_add_scan","contacts_add_shake"]
        for i in 0..<4 {
            let button=ImageButton(frame: CGRect(x: 0,y: (CGFloat(i) * CGFloat((optionsView!.height-17)/4))+10,width: optionsView!.width,height: CGFloat((optionsView!.height-17)/4)))
            button.setImageAndTitle(UIImage(named: images[Int(i)])!, title: titles[Int(i)])
            optionsView?.addSubview(button)
            button.tag=Int(i)
            button.addTarget(self, action: #selector(ChatViewController.buttonClick(_:)), for: UIControlEvents.touchUpInside)
            let line=UIImageView(frame: CGRect(x: 10,y: button.bottom,width: optionsView!.width-20,height: 1))
            line.image=UIImage(named: "MoreFunctionFrameLine")
            optionsView?.addSubview(line)
        }
    }
    
    // MARK: 创建tableView
    func initTableView(){
        
        tableView=UITableView(frame: self.view.frame, style: UITableViewStyle.plain)
        tableView?.delegate=self
        tableView?.dataSource=self
        self.view.addSubview(tableView!)
        tableView?.separatorStyle=UITableViewCellSeparatorStyle.none
        
    }

    // MARK: 按钮事件点击
    func buttonClick(_ sender:UIButton){
        
        switch sender.tag {
        case 0:
            print("发起群聊")
            break;
        case 1:
            print("添加好友")
            self.navigationController?.pushViewController(AddFriendViewController(), animated: true)
            break;
            
        case 2:
            print("扫一扫")
            break;
        default:
            print("收钱")
            break;
        }
        
        addContact()
    }
    
    
    /**
     * 添加控件出现
     */
    func addContact(){
        
        if optionsView?.alpha==0 {
            optionsView?.alpha=1
        }else{
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.optionsView?.alpha=0
                self.optionsView?.frame=CGRect(x: self.view.width-100-5,y: 64,width: 100,height: 140)
                
                }, completion: { (isOk:Bool) -> Void in
                    if isOk {
                        self.optionsView?.frame=CGRect(x: self.view.width-140-5,y: 64,width: 140
                            ,height: 190)
                    }
            })
        }
    }
    
    //MARK:tableViewDelegate tableViewDataSouce
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //返回数据库聊天列表数组的个数
        return ChatListManager.sharedManager.selectAllChatLists().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "chatlist_cell") as? ChatTableViewCell
        
        if cell == nil {
            
            cell=ChatTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "chatlist_cell")
        }
        
        let chatObject = ChatObject()
        
        let message = ChatListManager.sharedManager.selectAllChatLists()[indexPath.row]
        
        chatObject.content=message.content
        chatObject.imageUrl="defultIcon"
        chatObject.time=message.time?.stringValue
        chatObject.title="小分队"
        
        cell?.chat=chatObject
        return cell!
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.tabBarController?.tabBar.isHidden=true
        
        let chatDeatilVC = ChatDeatilViewController()
        
        self.navigationController?.pushViewController(chatDeatilVC, animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        
        return UIStatusBarStyle.lightContent
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(UpDateChatList), object: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
