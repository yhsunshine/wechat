//
//  ContactInfoViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/12/10.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class ContactInfoViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var tableView:UITableView?
    var userJid:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor=Tools.colorWith(239, G: 239, B: 244)
        
        self.navigationController?.navigationBar.tintColor=UIColor.white
        
        self.title="详细资料"
        
        self.navigationItem.backBarButtonItem=UIBarButtonItem(title: "通讯录 ", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactInfoViewController.backButton))
        
        initUI()
        
      
        // Do any additional setup after loading the view.
    }
    
    func initUI(){
        
        tableView=UITableView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height), style: UITableViewStyle.grouped)
        self.view.addSubview(tableView!)
        tableView?.delegate=self
        tableView?.dataSource=self
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        let footView=UIView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: 100))
//        footView.backgroundColor=UIColor.blackColor()
        tableView?.tableFooterView=footView
        
        let addFriendButton=UIButton(frame: CGRect(x: 20,y: 10,width: self.view.width-40,height: 44));
        footView.addSubview(addFriendButton);
        
        var image=UIImage(named: "fts_green_btn");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        addFriendButton.setBackgroundImage(image, for: UIControlState())
        
        image=UIImage(named: "fts_green_btn_HL");
        image=image?.stretchableImage(withLeftCapWidth: NSInteger(image!.size.width/2), topCapHeight:NSInteger(image!.size.height/2))
        addFriendButton.setBackgroundImage(image, for: UIControlState.highlighted)
        
        //        addFriendButton.titleLabel?.font=UIFont(name: FontFamilyName, size: 17);
        addFriendButton.titleLabel?.font=UIFont.systemFont(ofSize: 17)
        addFriendButton.setTitle("发起聊天", for: UIControlState());
        addFriendButton.setTitleColor(UIColor.white, for: UIControlState());
        addFriendButton.addTarget(self, action: #selector(ContactInfoViewController.startToTalk), for: UIControlEvents.touchUpInside);
        addFriendButton.layer.cornerRadius=5;
        addFriendButton.layer.masksToBounds=true;

    }
    
    /**
     * 发起聊天
     */
    func startToTalk(){
    
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var index:Int
        
        switch section {
            
        case 0:
            index=1
            break;
        case 1:
            index=1
            break;
        default:
            index=3
            break;
        }
        return index
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0{
            
            let imageView=UIImageView(frame: CGRect(x: 13, y: 10, width: 70, height: 70))
            cell.contentView.addSubview(imageView)
            imageView.image=UIImage(named: "defultIcon")
            imageView.contentMode=UIViewContentMode.scaleAspectFit
            imageView.layer.cornerRadius=5
            imageView.layer.masksToBounds=true
            
            let username = UILabel(frame: CGRect(x: imageView.right+5, y: imageView.top, width: 100, height: 20))
            username.text=userJid
            cell.contentView.addSubview(username)
            
        }else if (indexPath as NSIndexPath).section == 1 && (indexPath as NSIndexPath).row == 0{
            
            cell.textLabel?.text="设置备注跟标签"
            
        }else if (indexPath as NSIndexPath).section == 2 {
            
            if (indexPath as NSIndexPath).row == 0{
                cell.textLabel?.text="地区"
                cell.detailTextLabel?.text="湖北"
            }else if (indexPath as NSIndexPath).row == 1{
                cell.textLabel?.text="个人照片"
            }else if (indexPath as NSIndexPath).row == 2{
                cell.textLabel?.text="更多"
            }
        }
//        cell.selectionStyle=UITableViewCellSelectionStyle.Default
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat?
        
        if (indexPath as NSIndexPath).section == 0 && (indexPath as NSIndexPath).row == 0{
            
            height=90
        }else if (indexPath as NSIndexPath).section == 2 && (indexPath as NSIndexPath).row == 1{
            height=80
        }else{
            height=44
        }
        
        return height!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func backButton(){
        
        self.tabBarController?.tabBar.isHidden=false
        
        self.navigationController!.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
    }
    
    func 💓(_ 👻:String,😊:String){
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
