//
//  ContactViewController.swift
//  WeChat
//
//  Created by yh_SunShine on 15/11/29.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var tableView:UITableView?
    var contacts:Array<User>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title="联系人"
        
        NotificationCenter.default.addObserver(self, selector: #selector(ContactViewController.updateContacts), name: NSNotification.Name(rawValue: UpDateContactList), object: nil)
        
       contacts=ContactManager.sharedManager.selectAllContacts(UserDefaults.standard.object(forKey: "my_jid") as! String)
       
        initUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    func updateContacts(){
    
        contacts=ContactManager.sharedManager.selectAllContacts(UserDefaults.standard.object(forKey: "my_jid") as! String)
        tableView?.reloadData()
    }
    
    func initUI(){
        
        
        
        tableView=UITableView(frame: self.view.frame, style: UITableViewStyle.plain)
        self.view.addSubview(tableView!)
        tableView?.delegate=self;
        tableView?.dataSource=self;
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        print(tableView)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contacts!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID="cell"
        let cell=tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        let user=contacts![(indexPath as NSIndexPath).row]
        cell.textLabel?.text=user.contact!.jid
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tabBarController?.tabBar.isHidden=true
        
        let user=contacts![(indexPath as NSIndexPath).row]
        let contactInfoViewController=ContactInfoViewController()
        contactInfoViewController.userJid=user.contact!.jid
        self.navigationController?.pushViewController(contactInfoViewController, animated: true)
        
    }
    
    deinit{
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: UpDateContactList), object: nil)
        
    }
    
}
