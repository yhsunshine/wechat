//
//  Define_header.h
//  WeChat
//
//  Created by yh_SunShine on 15/11/26.
//  Copyright © 2015年 yhsunshine. All rights reserved.
//

#ifndef Define_header_h
#define Define_header_h

/**
 * 通知标识符
 */
#define WeChatLoginSuccess @"WeChat_login_success"
#define UpDateContactList  @"UpDate_contact_list"

#define UpDateChatList     @"UpDate_chat_list"

#define FontFamilyName @"HelveticaNeueLTPro-Lt"
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

#endif /* Define_header_h */

