//
//  ChatTableViewCell.swift
//  WeChat
//
//  Created by yh_SunShine on 16/9/8.
//  Copyright © 2016年 yhsunshine. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    var headerPortrait:UIImageView?
    var content:UILabel?
    var title:UILabel?
    var bottmLine:UIView?
    
    var chat:ChatObject{
    
        set{
        
            self.headerPortrait?.image=UIImage(named: newValue.imageUrl!)
            
            self.title?.text=newValue.title
            
            self.content?.text=newValue.content
        }
        
        get{
        
            return self.chat
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        createUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createUI() -> Void {
        
        headerPortrait=UIImageView()
        contentView.addSubview(headerPortrait!)
        
        content=UILabel()
        contentView.addSubview(content!)
        content?.textColor=UIColor.lightGray
        content?.font=UIFont.systemFont(ofSize: 13)
        
        title=UILabel()
        contentView.addSubview(title!)
        title?.font=UIFont.systemFont(ofSize: 18)
        
        bottmLine=UIView()
        bottmLine?.backgroundColor=hexColor("#d7d7d9")
        contentView.addSubview(bottmLine!)
    }
    
    override func layoutSubviews() {
        
        let frame = self.contentView.frame
        
        headerPortrait?.frame=CGRect(x: 7.5, y: 7.5, width: frame.height-15, height: frame.height-15)
        headerPortrait?.layer.cornerRadius=5.0
        headerPortrait?.layer.masksToBounds=true
        
        title?.frame=CGRect(x: (headerPortrait?.right)!+10, y: (headerPortrait?.top)!, width: 100, height: 25)
        
        content?.frame=CGRect(x: (title?.left)!, y: (title?.bottom)!, width: frame.size.width, height: (headerPortrait?.height)!-(title?.height)!)
        
        bottmLine?.frame=CGRect(x: 0, y: self.contentView.bottom-0.5, width: self.contentView.width, height: 0.5)
        
    }

}
